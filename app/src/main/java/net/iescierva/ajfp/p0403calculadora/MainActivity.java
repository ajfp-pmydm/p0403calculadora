package net.iescierva.ajfp.p0403calculadora;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import net.iescierva.ajfp.p0403calculadora.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {
    private double op1=0;
    private boolean op1active=false;
    private static int maxlendisplay=20;
    //***data binding
    private ActivityMainBinding dataBind;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // *** Inflate the content view (replacing `setContentView`)
        dataBind = DataBindingUtil.setContentView(this, R.layout.activity_main);
        //setContentView(R.layout.activity_main);

        View.OnClickListener listener=new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button pressedButton =(Button)v;  //pressed button
                String c= pressedButton.getText().toString();  //text from pressed button
                //TextView d=findViewById(R.id.displayTextView); //"standard" display
                //perform activity
                switch (c) {
                    case "C":
                        dataBind.displayTextView.setText("0");
                        break;
                    case "AC":
                        op1active = false;
                        op1=0;
                        dataBind.displayTextView.setText("0");
                        dataBind.opTextView.setText("");
                        break;
                    case "0":
                    case "1":
                    case "2":
                    case "3":
                    case "4":
                    case "5":
                    case "6":
                    case "7":
                    case "8":
                    case "9":
                        if (dataBind.opTextView.getText().toString().equals("=")) {
                            dataBind.displayTextView.setText("");
                            dataBind.opTextView.setText("");
                        }
                        if (dataBind.displayTextView.getText().toString().equals("0"))
                            dataBind.displayTextView.setText(c);
                        else if (dataBind.displayTextView.getText().toString().length()<maxlendisplay)
                            dataBind.displayTextView.setText(dataBind.displayTextView.getText()+c);
                        break;
                    case ".":
                        if (dataBind.dotButton.isEnabled()) {
                            dataBind.displayTextView.setText(dataBind.displayTextView.getText()+".");
                            dataBind.dotButton.setEnabled(false);
                        }
                        break;
                    case "E":
                        if (dataBind.eButton.isEnabled()) {
                            dataBind.displayTextView.setText(dataBind.displayTextView.getText()+"e");
                            dataBind.eButton.setEnabled(false);
                            dataBind.dotButton.setEnabled(false);
                        }
                        break;

                    case "+":
                    case "-":
                    case "x":
                    case "/":
                        dataBind.opTextView.setText(c);
                        op1=Double.parseDouble(dataBind.displayTextView.getText().toString());
                        dataBind.displayTextView.setText("");
                        op1active = true;
                        dataBind.dotButton.setEnabled(true);
                        dataBind.eButton.setEnabled(true);
                        break;
                    case "=":
                        String str= dataBind.displayTextView.getText().toString();
                        if (op1active) {
                            try {
                                switch (dataBind.opTextView.getText().toString()) {
                                    case "+":
                                        op1 += Double.parseDouble(str);
                                        break;
                                    case "-":
                                        op1 -= Double.parseDouble(str);
                                        break;
                                    case "x":
                                        op1 *= Double.parseDouble(str);
                                        break;
                                    case "/":
                                        op1 /= Double.parseDouble(str);
                                        break;
                                }
                                dataBind.opTextView.setText("=");
                                op1active = false;
                                dataBind.dotButton.setEnabled(true);
                                dataBind.eButton.setEnabled(true);
                                dataBind.displayTextView.setText(Double.toString(op1));
                                str= dataBind.displayTextView.getText().toString();
                                if (str.endsWith(".0"))
                                    dataBind.displayTextView.setText(str.substring(0, str.length() - 2));
                            } catch (Exception e)
                            {
                                dataBind.displayTextView.setText("Error");
                            }
                        }
                        break;
                }
            }
        };

        /*
       Button b0=findViewById(R.id.button0);
       Button b1=findViewById(R.id.button1);
       Button b2=findViewById(R.id.button2);
       Button b3=findViewById(R.id.button3);
       Button b4=findViewById(R.id.button4);
       Button b5=findViewById(R.id.button5);
       Button b6=findViewById(R.id.button6);
       Button b7=findViewById(R.id.button7);
       Button b8=findViewById(R.id.button8);
       Button b9=findViewById(R.id.button9);
       Button bDot=findViewById(R.id.dotButton);
       Button bE=findViewById(R.id.eButton);
       Button bPlus=findViewById(R.id.plusButton);
       Button bMinus=findViewById(R.id.minusButton);
       Button bTimes=findViewById(R.id.timesButton);
       Button bDivide=findViewById(R.id.divideButton);
       Button bC=findViewById(R.id.cButton);
       Button bCE=findViewById(R.id.ceButton);
       Button bPc=findViewById(R.id.pcButton);
       Button bEq=findViewById(R.id.eqButton);
        */

        //El databinding lo usamos para acceder a todos los objetos que hay definidos en
        //activity_main

        dataBind.button0.setOnClickListener(listener);
        dataBind.button1.setOnClickListener(listener);
        dataBind.button2.setOnClickListener(listener);
        dataBind.button3.setOnClickListener(listener);
        dataBind.button4.setOnClickListener(listener);
        dataBind.button5.setOnClickListener(listener);
        dataBind.button5.setOnClickListener(listener);
        dataBind.button6.setOnClickListener(listener);
        dataBind.button7.setOnClickListener(listener);
        dataBind.button8.setOnClickListener(listener);
        dataBind.button9.setOnClickListener(listener);
        dataBind.eButton.setOnClickListener(listener);
        dataBind.plusButton.setOnClickListener(listener);
        dataBind.minusButton.setOnClickListener(listener);
        dataBind.timesButton.setOnClickListener(listener);
        dataBind.divideButton.setOnClickListener(listener);
        dataBind.cButton.setOnClickListener(listener);
        dataBind.ceButton.setOnClickListener(listener);
        dataBind.pcButton.setOnClickListener(listener);
        dataBind.eqButton.setOnClickListener(listener);
    }
}